#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[3]:


Branch1 = pd.read_excel(r'C:\Users\KARMEGAM\Documents\Branch1.xlsm')
Branch1.head()


# In[5]:


Branch2 = pd.read_excel(r'C:\Users\KARMEGAM\Documents\Branch2.xlsm')
Branch2.head()


# In[9]:


sales = pd.concat([Branch1,Branch2])
sales["PRODUCT_TYPE"] = sales["PRODUCT_TYPE"].str.lower()
sales.head()


# In[13]:


output = sales.melt(id_vars=["PRODUCT_TYPE"], 
        var_name="SOLD_DATE", 
        value_name="QTY_SOLD")
output


# In[14]:


output = output[['SOLD_DATE', 'PRODUCT_TYPE', 'QTY_SOLD']]
output


# In[15]:


final = output.groupby(['SOLD_DATE','PRODUCT_TYPE'],as_index=False).agg({'QTY_SOLD' : sum })
final


# In[18]:


final['temp_index'] = pd.to_datetime(final['SOLD_DATE'], format = '%m %d, %Y')


# In[19]:


final = final.sort_values(by=['temp_index','PRODUCT_TYPE']) 


# In[20]:


final


# In[21]:


final = final.drop(columns=['temp_index'])
final


# In[23]:


final.to_csv(r'C:\Users\KARMEGAM\Documents\saba.csv',index=False)


# In[ ]:





# In[ ]:




