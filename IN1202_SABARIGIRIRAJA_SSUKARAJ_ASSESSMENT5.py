#!/usr/bin/env python
# coding: utf-8
import requests
import json

jan= requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-01-01&endtime=2017-01-31")
jan.json()
feb=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-02-01&endtime=2017-02-28")
feb.json()
mar=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-03-01&endtime=2017-03-31")
mar.json()
apr=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-04-01&endtime=2017-04-30")
apr.json()
may=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-05-01&endtime=2017-05-31")
may.json()
jun=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-06-01&endtime=2017-06-30")
jun.json()
jul=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-07-01&endtime=2017-07-31")
jul.json()
aug=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-08-01&endtime=2017-08-31")
aug.json()
sep=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-09-01&endtime=2017-09-30")
sep.json()
octo=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-10-01&endtime=2017-10-31")
octo.json()
nov=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-11-01&endtime=2017-11-30")
nov.json()
dec=requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-12-01&endtime=2017-12-31")
dec.json()


# In[43]:


a=[]
a.extend(may.json()['features'])


# In[60]:


a.extend(jan.json()['features'])


# In[63]:


a.extend(feb.json()['features'])


# In[58]:


a.extend(mar.json()['features'])


# In[44]:


a.extend(apr.json()['features'])


# In[46]:


a.extend(jun.json()['features'])


# In[47]:


a.extend(jul.json()['features'])


# In[48]:


a.extend(aug.json()['features'])


# In[49]:


a.extend(sep.json()['features'])


# In[50]:


a.extend(octo.json()['features'])


# In[51]:


a.extend(nov.json()['features'])


# In[52]:


a.extend(dec.json()['features'])


# In[64]:


a
len(a)


# In[68]:


import pandas as pd


# In[69]:


df = pd.json_normalize(a)


# In[71]:


df.columns


# In[72]:


df


# In[73]:


df.rename(columns ={'properties.mag':'mag', 'properties.place':'place', 'properties.time':'time',
       'properties.updated':'updated', 'properties.tz':'tz', 'properties.url':'url',
       'properties.detail':'detail', 'properties.felt':'felt','properties.cdi':'cdi',
       'properties.mmi':'mmi', 'properties.alert':'alert', 'properties.status':'status',
       'properties.tsunami':'tsunami', 'properties.sig':'sig', 'properties.net':'net',
       'properties.code':'code', 'properties.ids':'ids', 'properties.sources':'sources',
       'properties.types':'types',  'properties.nst':'nst', 'properties.dmin':'dmin',
       'properties.rms':'rms', 'properties.gap':'gap', 'properties.magType':'magType',
       'properties.type':'ptype', 'properties.title':'title', 'geometry.type':'gtype',
       'geometry.coordinates':'coordinates'},inplace = True)


# In[74]:


df = df.drop(columns='type')


# In[75]:


max(df['mag'])


# In[77]:


import pyodbc 
server = 'OTUSDPSQL' 
database = 'Target_PY_SSukaraj' 
username = 'PY_SSukaraj' 
password = '' 
cnxn = pyodbc.connect('DRIVER={SQL Server Native Client 11.0};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()


# In[78]:


df.columns


# In[79]:


cursor.execute('CREATE TABLE dbo.earthquake                ( "id" Varchar(255)                , "mag" Varchar(255)                , "place" Varchar(255)                , "time" Varchar(255)                , "updated" Varchar(255)                , "tz" Varchar(255)                , "url" Varchar(255)                , "detail" Varchar(250)                , "felt" Varchar(250)                , "cdi"  Varchar(250)                , "mmi" Varchar(250)                , "alert" Varchar(250)                , "status" Varchar(250)                , "tsunami" Varchar(250)                , "sig" Varchar(250)                , "net" Varchar(250)                , "code" Varchar(250)                , "ids" Varchar(250)                , "sources" Varchar(250)                , "types" Varchar(250)                , "nst" Varchar(250)                , "dmin" Varchar(250)                , "rms" Varchar(250)                , "gap" Varchar(250)                , "magType" Varchar(250)                , "ptype" Varchar(250)                , "title" Varchar(250)                , "gtype" Varchar(250)                , "coordinates" Varchar(250)               )')

cnxn.commit()
cursor.close()


# In[80]:


print(pd.read_sql('select * from dbo.earthquake',cnxn))


# In[82]:


df = df.astype(str)


# In[83]:


import sqlalchemy
import urllib
 

quoted = urllib.parse.quote_plus('Driver={SQL Server Native Client 11.0};'
                'Server=OTUSDPSQL;'
                'Database=Target_PY_SSukaraj;'
                'UID=PY_SSukaraj;'
                'PWD='
                )
engine=sqlalchemy.create_engine('mssql+pyodbc:///?odbc_connect={}'.format(quoted))


# In[ ]:


df.to_sql('earthquakes',con=engine,if_exists='replace',index=False)


# In[ ]:




